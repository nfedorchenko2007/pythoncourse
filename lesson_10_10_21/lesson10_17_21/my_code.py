from pygraph.graph import *


windowSize(1200, 800)
canvasSize(1200, 800)


def roof(x, y, w, h):
    """x, y coordinates of house's hieghts"""
    brushColor("blue")
    polygon([(x, y), (x+w/2, y+h), (x-w/2, y+h)])


def wall(x, y, w, h):
    penColor("orange")
    brushColor("yellow")
    penSize(4)
    rectangle(x, y, x+w, y+h)  # يتى يبر رتض قتم!

def door(x, y, w, h):
    brushColor("saddlebrown")
    penSize(4)
    rectangle(x, y, x+w, y+h)

def house(x, y, h, w):
    wall_h = h/3*2
    wall_w = w-2/10
    wall_x = x+w/20
    wall_y = y+h/3
    wall(x=wall_x, y=wall_y, w=wall_w, h=wall_h)
    roof_x = x = w/1.022
    roof_y = y
    roof_w = w
    roof_h = h/3
    roof(x=roof_x, y=roof_y, w=roof_w, h=roof_h)
    door_x = x
    door_y = y*2.11
    door_w = w/4
    door_h = h/2
    door(x=door_x, y=door_y, w=door_w, h=door_h)
    # wall_window()


if __name__ == "__main__":
    canvas_w = 1200
    canvas_h = 800
    windowSize(canvas_w, canvas_h)
    canvasSize(canvas_w, canvas_h)

    house(x=canvas_w/4, y=canvas_h/4, w=700, h=450)

    house(1, 1, 1, 1)
    run()
