from pygraph.graph import *


def printWindow(x, y, xSize=30, ySize=30):
    brushColor(173, 216, 230)
    rectangle(x, y, x+xSize, y+ySize)
    line(x, y+ySize/2, x+xSize, y+ySize/2)
    line(x+xSize/2, y, x+xSize/2, y+ySize)


def printMainPart(x, y, xSize=200, ySize=200):
    brushColor(134, 136, 138)
    rectangle(x, y, x+xSize, y+ySize)


def printRoof(x, y, xSize=330, ySize=150):
    brushColor(255, 94, 19)
    polygon([(x, y+ySize), (x+xSize/2, y), (x+xSize, y+ySize)])


def printDoor(x, y, xSize=60, ySize=100):
    brushColor(255, 255, 0)
    rectangle(x, y, x+xSize, y+ySize)
    add = ySize/9
    for i in range(1, 10):
        line(x, y+add*i, x+xSize, y+add*(i-1))
    brushColor(0, 0, 0)
    circle(x+xSize/6, y+ySize/2, xSize/20)


def printRoofWindow(x, y, rSize=40):
    circle(x, y, rSize)
    line(x , y-rSize, x, y+rSize)
    line(x-rSize , y, x+rSize, y)


def printChimney(x, y, xSize=30, ySize=60):
    brushColor(150, 75, 0)
    mainPartW = xSize*0.8
    mainPartH = y+ySize
    mainPartX = x + mainPartW
    mainPartY = y + mainPartH
    rectangle(x, y, x+xSize, y+ySize)
    brickH = ySize/18
    rectangle(x-xSize/3, y-brickH, x+xSize+xSize/3, y+brickH)
    # for i in range(3):
    #     line(x, y+brickH+(ySize-brickH)/3*i, x+xSize, y+brickH+(ySize-brickH)/3*i)
    # line(x+xSize*2/3, y+brickH, x+xSize*2/3, y+brickH+(ySize-brickH)/3)
    # line(x+xSize/3, y+brickH+(ySize-brickH)/3, x+xSize/3, y+brickH+(ySize-brickH)/3*2)
    # line(x+xSize*2/3, y+brickH+(ySize-brickH)/3*2, x+xSize*2/3, y+brickH+(ySize-brickH)/3*3)
    # line(x+xSize/2, y-brickH, x+xSize/2, y+brickH)


def printHouse(x, y, xSize=330, ySize=350):
    wallX = x+xSize*0.05
    wallY = y+ySize/3
    wallW = xSize*0.9
    wallH = ySize*2/3
    printMainPart(wallX, wallY, wallW, wallH)

    chimneyX = x+xSize*0.75
    chimneyY = y+ySize*0.1
    chimneyW = xSize*0.1
    chimneyH = ySize*0.20
    printChimney(chimneyX, chimneyY, chimneyW, chimneyH)

    roofX = x
    roofY = y
    roofW = xSize
    roofH = ySize/3
    printRoof(roofX, roofY, roofW, roofH)


    window1W = min([xSize, ySize])*0.2
    window1H = min([xSize, ySize])*0.2
    window1X = x+xSize*0.2-window1W/2
    window1Y = y+ySize*2/3-window1H/2
    printWindow(window1X, window1Y, window1W, window1H)
    
    window2W = min([xSize, ySize])*0.2
    window2H = min([xSize, ySize])*0.2
    window2X = x+xSize*0.8-window2W/2
    window2Y = y+ySize*2/3-window2H/2
    printWindow(window2X, window2Y, window2W, window2H)

    roofWindowX = x+xSize/2
    roofWindowY = y+ySize*1/6
    roofWindowR = min([ySize, xSize])/12
    printRoofWindow(roofWindowX, roofWindowY, roofWindowR)
    
    doorW = xSize*0.2
    doorH = ySize*4/9
    doorX = x+xSize/2-doorW/2
    doorY = y+ySize-doorH
    printDoor(doorX, doorY, doorW, doorH)

if __name__ == "__main__":
    windowSize(1200, 1350)
    canvasSize(1200, 1350)
    #printHouse(75, 75, 450, 525)
    printHouse(75, 75, 450*2, 525*2)
    run()